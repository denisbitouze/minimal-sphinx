```{role} latexlogo
```
```{role} tttexlogo
```
# Comment obtenir des symboles électroniques ?

Les symboles électroniques n'existent pas sous forme de caractères mais sont bien disponibles d'une autre manière avec {latexlogo}`LaTeX`.

La question « {doc}`Comment obtenir des symboles de composants électroniques ? </4_domaines_specialises/electronique/symboles_electroniques>` » présente une solution.

```{eval-rst}
.. meta::
   :keywords: LaTeX,symbole,électronique
```
