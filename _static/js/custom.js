window.onload = function() {
    var latexlogos = document.querySelectorAll('.latexlogo'), i, j;

    for (i = 0; i < latexlogos.length; ++i) {
        let insidelatexlogo = '';
        for (j = 0; j < latexlogos[i].innerText.length; j++) {
            if (latexlogos[i].innerText[j] !== " ") {
                insidelatexlogo += "<insidelatexlogo>";
                insidelatexlogo += latexlogos[i].innerText[j];
                insidelatexlogo += "</insidelatexlogo>";
            }
        }
        latexlogos[i].innerHTML = insidelatexlogo;
    }

    var texlogos = document.querySelectorAll('.tttexlogo'), i, j;

    for (i = 0; i < texlogos.length; ++i) {
        let insidetexlogo = '';
        for (j = 0; j < texlogos[i].innerText.length; j++) {
            if (texlogos[i].innerText[j] !== " ") {
                insidetexlogo += "<insidetexlogo>";
                insidetexlogo += texlogos[i].innerText[j];
                insidetexlogo += "</insidetexlogo>";
            }
        }
        texlogos[i].innerHTML = insidetexlogo;
    }
};
