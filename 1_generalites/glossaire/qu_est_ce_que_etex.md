```{role} latexlogo
```
```{role} tttexlogo
```
# Qu'est-ce qu'ε-TeX ?

Les extensions ε-TeX de {tttexlogo}`TeX` sont une gamme de nouvelles fonctionnalités ajoutées par le {doc}`NTS </1_generalites/histoire/developpement_du_moteur_tex>` au programme {tttexlogo}`TeX` original de Donald Knuth.

Dans une distribution {tttexlogo}`TeX` moderne, les extensions sont activées pour presque tous les formats sauf {tttexlogo}`TeX` de Knuth lui-même. En tant que tel, la plupart des utilisateurs n'utiliseront jamais un système ne comportant pas ε-TeX : en effet, {latexlogo}`LaTeX` en a besoin depuis 2017.

______________________________________________________________________

*Sources :*

- {faquk}`What is ε-TeX? <FAQ-etex>`
- <https://www.tug.org/TUGboat/tb29-1/tb91reutenauer.pdf>

```{eval-rst}
.. meta::
   :keywords: LaTeX,histoire
```
