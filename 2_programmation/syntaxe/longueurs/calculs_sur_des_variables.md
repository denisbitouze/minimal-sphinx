```{role} latexlogo
```
```{role} tttexlogo
```
# Comment réaliser des calculs avec les variables LaTeX ?

- On peut multiplier une longueur par un nombre, en faisant précéder le nom du compteur par la valeur en question. Pour agrandir les marges sur une portion de texte, on pourra donc faire :

```latex
% !TEX noedit
\begin{center}
\begin{minipage}{0.8\textwidth}
...
\end{minipage}
\end{center}
```

Attention, cela ne fonctionne que pour les longueurs, pas pour les compteurs.

- {tttexlogo}`TeX` fournit également les commandes telles que `\multiply` et `\divide`. Elles servent à multiplier et diviser des dimensions et des compteurs. On peut donc écrire :

```latex
% !TEX noedit
\divide \longueur by 4
```

\\noindent pour diviser la valeur de `\longueur` par 4. La valeur de la longueur est directement mise à jour. Les additions et soustractions peuvent être effectuées avec `\addtolength`.

:::{important}
`\divide` et `\multiply` ne marchent pas avec des compteurs {latexlogo}`LaTeX` mais seulement avec des compteurs {tttexlogo}`TeX`. Cela vient du fait que les compteurs {latexlogo}`LaTeX` ne sont pas de simples compteurs mais des constructions qui gérent les références et dépendances de compteurs {tttexlogo}`TeX`.

Si l'on gère directement le compteur {tttexlogo}`TeX` `c@cptr` correspondant au compteur {latexlogo}`LaTeX` cptr, le mécanisme de dépendance n'est plus pris en compte
:::

- Le package {ctanpkg}`calc` permet de faire des calculs arithmétiques au niveau des commandes `\setlength`, `\addtolength`, `\setcounter` et `\addtocounter`.
- Le package {ctanpkg}`realcalc` permet quant à lui de faire des calculs sur des nombres réels, au niveau des variables.

```{eval-rst}
.. meta::
   :keywords: LaTeX
```
