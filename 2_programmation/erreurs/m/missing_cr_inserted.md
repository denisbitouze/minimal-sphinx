```{role} latexlogo
```
```{role} tttexlogo
```
# Que signifie l'erreur : « Missing `\cr` inserted » ?

- **Message** : `Missing \cr inserted`
- **Origine** : *TeX*.

{tttexlogo}`TeX` pense qu'il est temps de terminer une ligne d'un alignement et insère sa commande de bas niveau pour cela. Dans un document {latexlogo}`LaTeX`, cette supposition est normalement erronée et la tentative de réparation de {tttexlogo}`TeX` échoue le plus souvent.

______________________________________________________________________

*Sources :*

- <https://latex.developpez.com/faq/erreurs?page=M>,
- [LaTeX Companion, 2e édition](https://www.latex-project.org/help/books/#french), Frank Mittelbach, Michel Goossens, Johannes Braams, David Carlisle, Chris Rowley (Pearson, 2006) ; ISBN : 978-2-7440-7182-9. Annexe B, *Détecter et résoudre les problèmes*, reproduite avec l'aimable autorisation de l'éditeur.

```{eval-rst}
.. meta::
   :keywords: messages d'erreur de LaTeX,problème dans un tableau,tabular,array,matrice,alignement
```
